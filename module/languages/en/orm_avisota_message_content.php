<?php

/**
 * Avisota newsletter and mailing system
 * Copyright © 2016 Sven Baumann
 *
 * PHP version 5
 *
 * @copyright  way.vision 2016
 * @author     Sven Baumann <baumann.sv@gmail.com>
 * @package    avisota/contao-message-element-article
 * @license    LGPL-3.0+
 * @filesource
 */

/**
 * Fields
 */
$GLOBALS['TL_LANG']['orm_avisota_message_content']['newsId']       = array(
    'News',
    'Please choose the news to include.'
);
$GLOBALS['TL_LANG']['orm_avisota_message_content']['newsTemplate'] = array(
    'Template',
    'Please choose the news template.'
);
