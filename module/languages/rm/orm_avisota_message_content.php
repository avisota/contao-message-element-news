<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/rm/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-11T16:03:23+01:00
 */

$GLOBALS['TL_LANG']['orm_avisota_message_content']['newsId']['0']       = 'Novitads';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['newsId']['1']       = 'Tscherna las novitads per includer.';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['newsTemplate']['0'] = 'Template';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['newsTemplate']['1'] = 'Tscherna il template per las novitads.';
