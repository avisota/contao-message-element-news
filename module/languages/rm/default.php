<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/rm/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-11T16:03:22+01:00
 */

$GLOBALS['TL_LANG']['MCE']['news']['0'] = 'Novitads';
$GLOBALS['TL_LANG']['MCE']['news']['1'] = 'Mussa in artitgel da novitads.';
